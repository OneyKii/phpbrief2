<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="main.css">
    <title>Main</title>
</head>
<body>
    <div class="body">
        <div class="header">
            <h1 class="header-h1">Bookmark</h1>
        </div>

<?php

$user = 'root';
$pass = '';

try {
    // Connexion à MySQL
    $db = new PDO('mysql:host=localhost;dbname=bookmark_theo', $user, $pass);
    }
    // Test pour voir si la db est bien lié
 catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

// Récupération du contenue de link
$reponse = $db->query('SELECT*FROM link');

?>

<table>

<?php
while($donnees = $reponse->fetch()){
        echo '<tr>'; 
        echo '<td>'; 
        echo $donnees['name'];
        echo ' : ';
        echo '</td>'; 
        echo '<td>'; 
        echo $donnees['url'];
        echo '</td>';
        echo '</tr>'; 
}
?>

</table>

<?php

// Fin de la requète
$reponse-> closeCursor();

?>

<?php



?>



</div>
</body>
</html>